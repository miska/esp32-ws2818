/* Console example — WiFi commands

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "cmd_decl.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "cmd_gpio.h"

/** Arguments used by 'gpio' function */
static struct {
    struct arg_int *gpio;
    struct arg_int *value;
    struct arg_end *end;
} gpio_args;

static int gpio_set(int argc, char **argv) {
    int nerrors = arg_parse(argc, argv, (void **) &gpio_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, gpio_args.end, argv[0]);
        return 1;
    }

    ESP_LOGI(__func__, "Setting GPIO %d to %d", gpio_args.gpio->ival[0], gpio_args.value->ival[0]);

    GPIO_OUTPUT_SET(gpio_args.gpio->ival[0], gpio_args.value->ival[0]);

    return 0;
}

void register_gpio(void) {
    gpio_args.gpio = arg_int1(NULL, NULL, "<gpio>", "GPIO pin");
    gpio_args.value = arg_int1(NULL, NULL, "<value>", "desired value 1/0");
    gpio_args.end = arg_end(1);

    const esp_console_cmd_t gpio_cmd = {
        .command = "gpio",
        .help = "Set GPIO values",
        .hint = NULL,
        .func = &gpio_set,
        .argtable = &gpio_args
    };

    ESP_ERROR_CHECK( esp_console_cmd_register(&gpio_cmd) );
}
