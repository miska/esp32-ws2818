/* Console example — WiFi commands

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "cmd_decl.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "cmd_ws2812.h"
#include "ws2812.h"

TaskHandle_t ws2818_task = NULL;

static struct {
    struct arg_int *gpio;
    struct arg_int *length;
    struct arg_end *end;
} ws2818_config_args;

static struct {
    struct arg_str *color;
    struct arg_end *end;
} ws2818_color_args;

static struct {
    struct arg_str *colora;
    struct arg_str *colorb;
    struct arg_int *timing;
    struct arg_end *end;
} ws2818_blink_args;

static struct {
    struct arg_str *color;
    struct arg_int *timing;
    struct arg_end *end;
} ws2818_knight_args;

int ws2818_length;

rgbVal *pixels = NULL;

#define toRGB(rgb) makeRGBVal(((rgb) >> 16) & 0xff, ((rgb) >> 8) & 0xff, (rgb) & 0xff)
#define RGBDIV(rgb, div) \
    ( ((((rgb) & 0xff0000) / (div)) & 0xff0000) | \
      ((((rgb) & 0x00ff00) / (div)) & 0x00ff00) | \
       (((rgb) & 0x0000ff) / (div)) )

static int ws2818_setup(int argc, char **argv) {
    int nerrors = arg_parse(argc, argv, (void **) &ws2818_config_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, ws2818_config_args.end, argv[0]);
        return 1;
    }

    ESP_LOGI(__func__, "Setting up ws2818 on pin %d to length %d", ws2818_config_args.gpio->ival[0], ws2818_config_args.length->ival[0]);

    ws2812_init(ws2818_config_args.gpio->ival[0]);

    ws2818_length = ws2818_config_args.length->ival[0];

    if(pixels != NULL) {
        free(pixels);
    }

    pixels = malloc(sizeof(rgbVal) * ws2818_length);

    return 0;
}

static int alpha2num(char c) {
    if(c >= '0' && c <= '9')
        return c - '0';
    switch(c) {
        case 'a':
        case 'A':
            return 0xa;
            break;
        case 'b':
        case 'B':
            return 0xb;
            break;
        case 'c':
        case 'C':
            return 0xc;
            break;
        case 'd':
        case 'D':
            return 0xd;
            break;
        case 'e':
        case 'E':
            return 0xe;
            break;
        case 'f':
        case 'F':
            return 0xf;
            break;
        default:
            return -1;
    }
}

static uint32_t name2color(const char* name) {
    if(strcmp(name, "white") == 0)
        return 0xffffff;
    if(strcmp(name, "red") == 0)
        return 0xff0000;
    if(strcmp(name, "green") == 0)
        return 0x00ff00;
    if(strcmp(name, "blue") == 0)
        return 0x0000ff;
    if(strcmp(name, "yellow") == 0)
        return 0xffaa00;
    if(strcmp(name, "pink") == 0)
        return 0xff00ff;
    if(strcmp(name, "cyan") == 0)
        return 0x00ffff;
    if(strcmp(name, "orange") == 0)
        return 0xcc3300;
    if(strcmp(name, "cold") == 0)
        return 0xddddff;
    if(strcmp(name, "warm") == 0)
        return 0xff6611;

    uint32_t rgb = 0;
    if(strcmp(name, "random") == 0) {
        esp_fill_random(&rgb, sizeof(uint32_t));
        return rgb;
    }

    if(strlen(name) != 6) {
        ESP_LOGE(__func__, "Color has to have exactly six letters");
        return 1;
    }

    for(int i = 0; i<6; i++) {
        int c = alpha2num(name[i]);
        if(c <  0) {
            ESP_LOGE(__func__, "Invalid color format");
            return 1;
        }
        rgb <<= 4;
        rgb |= c;
    }

    return rgb;
}

int _ws2818_color(const char *color) {
    uint32_t rgb = 0;

    if((strcmp(color, "black") != 0) && (strcmp(color, "off") != 0) && (strcmp(color, "random") != 0)) {
        rgb = name2color(color);
    }

    if(strcmp(color, "random") == 0) {
        esp_fill_random(pixels, sizeof(rgbVal) * ws2818_length);
    } else {
        rgbVal pixel = toRGB(rgb);

        for(int i = 0; i < ws2818_length; i++) {
            pixels[i] = pixel;
        }
    }
    ws2812_setColors(ws2818_length, pixels);

    return 0;
}

static int ws2818_color(int argc, char **argv) {
    if(ws2818_task) {
        vTaskDelete(ws2818_task);
        ws2818_task = NULL;
    }
    int nerrors = arg_parse(argc, argv, (void **) &ws2818_color_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, ws2818_color_args.end, argv[0]);
        return 1;
    }

    ESP_LOGI(__func__, "Setting the color of ws2818 to %s", ws2818_color_args.color->sval[0]);

    return _ws2818_color(ws2818_color_args.color->sval[0]);
}

void ws2818_blink_task(void *arg) {
    ESP_LOGI(__func__, "Blinking ws2818 with colors %s, %s every %dms", ws2818_blink_args.colora->sval[0], ws2818_blink_args.colorb->sval[0], ws2818_blink_args.timing->ival[0]);
    while(true) {
        _ws2818_color(ws2818_blink_args.colora->sval[0]);
        vTaskDelay(ws2818_blink_args.timing->ival[0] / portTICK_PERIOD_MS);
        _ws2818_color(ws2818_blink_args.colorb->sval[0]);
        vTaskDelay(ws2818_blink_args.timing->ival[0] / portTICK_PERIOD_MS);
    }
}

void ws2818_knight_task(void *arg) {
    static int pos = 0;
    static int dir = 1;

    uint32_t rgb = name2color(ws2818_knight_args.color->sval[0]);
    rgbVal black = toRGB(0);
    rgbVal pixel = toRGB(rgb);
    rgb = RGBDIV(rgb, 16);
    rgbVal half_pixel = toRGB(rgb);

    ESP_LOGI(__func__, "Running knight rider mode with color %x(%x) and speed %d", pixel.num, half_pixel.num, ws2818_knight_args.timing->ival[0]);
    while(true) {
        for(int i = 0; i < ws2818_length; i++) {
            if((pos == i-1) || (pos == i+1))
                pixels[i] = half_pixel;
            else if(pos == i)
                pixels[i] = pixel;
            else
                pixels[i] = black;
        }

        ws2812_setColors(ws2818_length, pixels);

        pos += dir;
        if(pos >= ws2818_length)
            dir = -1;
        if(pos <= 0)
            dir = 1;
        vTaskDelay(ws2818_knight_args.timing->ival[0] / portTICK_PERIOD_MS);
    }
}


static int ws2818_blink(int argc, char **argv) {
    if(ws2818_task) {
       vTaskDelete(ws2818_task);
        ws2818_task = NULL;
    }
    int nerrors = arg_parse(argc, argv, (void **) &ws2818_blink_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, ws2818_blink_args.end, argv[0]);
        return 1;
    }

    return (xTaskCreate(ws2818_blink_task, "ws2818_task", configIDLE_TASK_STACK_SIZE, &ws2818_blink_args, tskIDLE_PRIORITY, &ws2818_task) != pdPASS);
}

static int ws2818_knight(int argc, char **argv) {
    if(ws2818_task) {
        vTaskDelete(ws2818_task);
        ws2818_task = NULL;
    }
    int nerrors = arg_parse(argc, argv, (void **) &ws2818_knight_args);
    if (nerrors != 0) {
        arg_print_errors(stderr, ws2818_blink_args.end, argv[0]);
        return 1;
    }

    return (xTaskCreate(ws2818_knight_task, "ws2818_task", configIDLE_TASK_STACK_SIZE, &ws2818_knight_args, tskIDLE_PRIORITY, &ws2818_task) != pdPASS);
}



void register_ws2818(void) {
    ws2818_config_args.gpio = arg_int1(NULL, NULL, "<gpio>", "GPIO pin");
    ws2818_config_args.length = arg_int1(NULL, NULL, "<length>", "length of the strip");
    ws2818_config_args.end = arg_end(1);
    ws2818_color_args.color = arg_str1(NULL, NULL, "<color>", "hexa/name representation of desired color");
    ws2818_color_args.end = arg_end(1);
    ws2818_blink_args.timing = arg_int1(NULL, NULL, "<timing>", "how long should be each color diplayed");
    ws2818_blink_args.colora = arg_str1(NULL, NULL, "<color>", "hexa/name representation of desired color");
    ws2818_blink_args.colorb = arg_str1(NULL, NULL, "<color>", "hexa/name representation of desired color");
    ws2818_blink_args.end = arg_end(1);
    ws2818_knight_args.timing = arg_int1(NULL, NULL, "<timing>", "how fast should be the effect (lower number = faster)");
    ws2818_knight_args.color = arg_str1(NULL, NULL, "<color>", "hexa/name representation of desired color");
    ws2818_knight_args.end = arg_end(1);

    const esp_console_cmd_t ws2818_setup_cmd = {
        .command = "ws2818_setup",
        .help = "Setup ws2818 LED strip",
        .hint = NULL,
        .func = &ws2818_setup,
        .argtable = &ws2818_config_args
    };

    const esp_console_cmd_t ws2818_color_cmd = {
        .command = "ws2818_color",
        .help = "Set ws2818 LED color",
        .hint = NULL,
        .func = &ws2818_color,
        .argtable = &ws2818_color_args
    };

    const esp_console_cmd_t ws2818_blink_cmd = {
        .command = "ws2818_blink",
        .help = "Start blinking ws2818 with specific colors",
        .hint = NULL,
        .func = &ws2818_blink,
        .argtable = &ws2818_blink_args
    };

    const esp_console_cmd_t ws2818_knight_cmd = {
        .command = "ws2818_knight",
        .help = "Start knight rider effect",
        .hint = NULL,
        .func = &ws2818_knight,
        .argtable = &ws2818_knight_args
    };

    ESP_ERROR_CHECK( esp_console_cmd_register(&ws2818_setup_cmd) );
    ESP_ERROR_CHECK( esp_console_cmd_register(&ws2818_color_cmd) );
    ESP_ERROR_CHECK( esp_console_cmd_register(&ws2818_blink_cmd) );
    ESP_ERROR_CHECK( esp_console_cmd_register(&ws2818_knight_cmd) );
}
